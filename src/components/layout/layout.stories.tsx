import React from "react";

import "../../styles/styles.scss";
import "bootstrap/dist/css/bootstrap.min.css";

import Header from "./header";

export default {
  title: "Layout Component",
  component: "Layout",
};

const header = (args) => <Header {...args} />;

export const UiHeader = header.bind([])