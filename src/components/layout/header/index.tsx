import React, { ReactElement, useState } from "react";
import Link from "next/link";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
} from "reactstrap";

//
export default function Header(): ReactElement {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
 return (
   <div>
     <Navbar dark expand="md" className="navbar">
       <Link href="/">
         <NavbarBrand>TZ</NavbarBrand>
       </Link>
       <NavbarToggler onClick={toggle} />
       <Collapse isOpen={isOpen} navbar>
         <Nav className="mr-auto" navbar>
           <NavItem className="navbar__item">
             <Link href="/home">
               <NavLink>Home</NavLink>
             </Link>
           </NavItem>
        
         </Nav>
       </Collapse>
     </Navbar>
   </div>
 );
}
