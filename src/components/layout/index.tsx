import React, { ReactElement, ReactNode } from "react";
import Footer from "./footer";
import Header from "./header";
import Head from "next/head";
interface Props {
  children: ReactNode;
}

export default function Layout({ children }: Props): ReactElement {
  return (
    <div>
        <Head>
            <title>Taher!</title>
        </Head>
      <Header />
      {children}
      <Footer />
    </div>
  );
}
