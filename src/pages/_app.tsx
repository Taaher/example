import type { AppProps /*, AppContext */ } from "next/app";
//import scss global and bootstrap
import "./../styles/styles.scss";
import "bootstrap/dist/css/bootstrap.min.css";
//import layout
import Layout from "./../components/layout/";

export default function MyApp({ Component, pageProps }: AppProps) {
  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>
  );
}
